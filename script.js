let contador = 0;

function encerrarJogo(vencedor){
	let casas = document.querySelectorAll(".casa");
	for (let i=0; i<casas.length; i++){
		casas[i].onclick = null;
	}

	let divVencedor = document.querySelector("#vencedor");
	if(vencedor){
		if(contador%2){
		divVencedor.innerHTML = "Jogador laranja venceu!" ;
			divVencedor.style = "color : #F65E01";
	}else{
		divVencedor.innerHTML = "Jogador azul venceu!" ;
		divVencedor.style = "color : #003399";
	}}
	else{
		divVencedor.innerHTML = "Deu bad" ;	
			}
		}
	

	function validas(c1, c2, c3){
    let cor1 = c1.style.backgroundColor;
    let cor2 = c2.style.backgroundColor;
    let cor3 = c3.style.backgroundColor;

    if(cor1 === "" || cor2 === "" || cor3 === ""){
        return false;
    }

    if(cor1 === cor2 && cor2 === cor3){
        return true;
    }

    return false;

}

function verificarExistenciaVencedor(){
    let casas = document.querySelectorAll(".casa");

    return( validas(casas[0], casas[1], casas[2]) ||
            validas(casas[3], casas[4], casas[5]) ||
            validas(casas[6], casas[7], casas[8]) ||
            validas(casas[0], casas[3], casas[6]) ||
            validas(casas[1], casas[4], casas[7]) ||
            validas(casas[2], casas[5], casas[8]) ||
            validas(casas[0], casas[4], casas[8]) ||
            validas(casas[2], casas[4], casas[6]) );

}

function jogar(){
    if(contador%2){
        this.style = "background-color:  #003399";
    }
    else{
        this.style = "background-color: #F65E01";
    }
	contador++;
	console.log("Numero de jogadas: "+contador);

    this.onclick = null;

    let vencedor = verificarExistenciaVencedor();
	console.log(vencedor);
	
	if(vencedor || contador === 9){
		encerrarJogo(vencedor);

	}
}

let casas = document.querySelectorAll(".casa");

for (let i=0; i<casas.length; i++){
    casas[i].onclick = jogar;
}

